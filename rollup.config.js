import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'

export default {
	input:'src/index.js',
  name:'app',
	output: {
    format:'iife',
    file:'public/js/bundle.js'
  },
  plugins:[
    resolve(),
    commonjs({
      include:['node_modules/**'],
    })
  ]
}
