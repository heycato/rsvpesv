const log = e => (console.log(e), e)
const err = e => (console.error(e), e)

const ESV_OPTS = [
  `include-passage-references=false`,
  `include-footnotes=false`,
  `include-footnotes-body=false`,
  `include-copyright=false`,
  `include-short-copyright=false`,
  `include-passage-horizontal-lines=false`,
  `include-heading-horizontal-lines=false`,
  `include-headings=false`,
  `include-subheadings=false`,
  `include-content-type=false`,
  `indent-paragraphs=0`,
  `indent-poetry=0`,
  `indent-declares=0`
]

const buildQuery = (book, ch) =>
  ESV_OPTS
  .concat(`q=${book}+${ch}`)
  .join('&')

const buildESVEndpoint = (book, ch) =>
  `https://api.esv.org/v3/passage/text/?${buildQuery(book, ch)}`

const mode = 'no-cors'

const headers = new Headers({ 
  Accept:'application/json',
  Authorization:'Token 6fbd080f49469fe46ff6fa9064664f8a46955edf'
})

const buildRequest = ({book, chapter}) =>
  new Request(buildESVEndpoint(book, chapter), {headers, method:'GET'})

const getPlayUpdate = ({verses, verse, word}) =>
  verses[verse][word + 1] ? { verse, word: word + 1 } :
  verses[verse + 1]       ? { verse: verse + 1, word: 0 } :
  { verse, word }

const getRewindUpdate = ({verses, verse, word}) =>
  verses[verse][word - 1] ? { verse, word: word - 1 } :
  verses[verse - 1]       ? { verse: verse - 1, word: verses[verse - 1].length - 1 } :
  { verse, word }

const getUpdateSpeed = wpm => 60000 / wpm

const hasVerseNumber = s => /^\[/.test(s)

const splitAroundCenter = (str, length) => ([
  str.slice(0, length / 2),
  str.slice(length / 2, length / 2 + 1),
  str.slice(length / 2 + 1, length)
])

const last = xs => xs[xs.length - 1]

const removeNewLine = str => str.replace(/\n/g, '')

const tokenize = (acc, word) =>
  hasVerseNumber(word) ?
    (acc.push([splitAroundCenter(word, word.length)]), acc) :
    appendToLast(acc, splitAroundCenter(word, word.length))

const appendToLast = (xs, element) =>
  ((last(xs) || []).push(element), xs)

const removeVerseNumbers = verse =>
  verse.filter(token => token.indexOf('[') === -1)

const processText = str =>
  str.split(' ')
  .filter(word => word !== '')
  .map(removeNewLine)
  .reduce(tokenize, [])
  .map(removeVerseNumbers)

export default {
  setWPM: (state, actions, {target:{value}}) => ({wpm:value}),
  menuToggle: (state, actions) => ({ menuOpen: !state.menuOpen }),
  select: (state, actions, book) => ({ selected: book }),
  play: (state, actions) => ({
    interval: setInterval(() => {
      actions.updateDisplay(getPlayUpdate(state))
    }, getUpdateSpeed(state.wpm))
  }),
  stop: (state, actions) => ({
    interval: clearInterval(state.interval)
  }),
  rewind: (state, actions) => ({
    interval: setInterval(() => {
      actions.updateDisplay(getRewindUpdate(state))
    }, getUpdateSpeed(state.wpm))
  }),
  updateBook: (state, actions, {book, chapter, verses}) => log({
    book, chapter, verses, verse:0, word:0, menuOpen: false
  }),
  updateDisplay: (state, actions, {verse, word}) => ({ verse, word }),
  fetchBook: (state, actions, data) => {
    fetch(buildRequest(data))
    .then(r => r.json())
    .then(({passages}) => passages[0])
    .then(blob => ({
      book:data.book, chapter:data.chapter, verses:processText(blob)
    }))
    .then(actions.updateBook)
    .catch(err)
  }
}
