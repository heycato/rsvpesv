import { app } from 'hyperapp'
import actions from './actions'
import view from './views'

const err = e => (console.error(e), e)

const state = {
  wpm: 250,
  book:'',
  chapter:0,
  verses:[[['RSV', 'P', 'ESV']]],
  verse:0,
  word:0
}

const booksData = fetch('data.json')

const init = books =>
  app({
    view,
    state:((state.books = books), state),
    actions
  })

window.onload = () =>
  booksData
  .then(r => r.json())
  .then(init)
  .catch(err)
