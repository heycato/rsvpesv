import { button, div } from '@hyperapp/html'

export default (state, actions) => 
  div({id:'menu', class: state.menuOpen ? 'show' : 'hide'},
    Object.keys(state.books)
    .map(book =>
      div({class:'menu-container'}, [
        button({
          class:'menu-book-button',
          onclick:() => actions.select(book),
        }, book),
        div({class:`menu-chapters ${state.selected === book ? 'show' : 'hide'}`},
          Array.from(Array(state.books[book]))
          .map((_, i) => i + 1)
          .map(chapter => 
            button({
              class:'menu-chapter-button button-clear',
              onclick:() => actions.fetchBook({book, chapter})
            }, chapter))
        )
      ])
    )
  )
