import { div, span } from '@hyperapp/html'

export default (state, actions) => {
  let word = state.verses[state.verse][state.word]
  return div({key:'display', id:'display'}, [
    span({id:'display-start'}, word[0]),
    span({id:'display-focus'}, word[1]),
    span({id:'display-end'}, word[2])
  ])
}
