import { main } from '@hyperapp/html'
import progress from './progress'
import display from './display'
import toolbar from './toolbar'
import menu from './menu'

export default (state, actions) =>
  main({id:'main'}, [
    progress(state, actions),
    display(state, actions),
    toolbar(state, actions),
    menu(state, actions)
  ])
