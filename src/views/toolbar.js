import { button, div } from '@hyperapp/html'

const preventDefaultThen = (fn) => (e) =>
  (e.preventDefault(), fn(e))

export default (state, actions) => {
  let menuBtnLabel = state.book ?
    `${state.book} ${state.chapter}:${state.verse + 1}` :
    'Menu'
  return div({key:'toolbar', id:'toolbar', class: state.interval ? 'fade' : ''}, [
    button({
      onmousedown: actions.rewind,
      onmouseup: actions.stop,
      ontouchstart: preventDefaultThen(actions.rewind),
      ontouchend: preventDefaultThen(actions.stop)
    }, '◄'),
    button({onclick: actions.menuToggle}, menuBtnLabel),
    button({
      onmousedown: actions.play,
      onmouseup: actions.stop,
      ontouchstart: preventDefaultThen(actions.play),
      ontouchend: preventDefaultThen(actions.stop)
    }, '►'),
  ])
}
