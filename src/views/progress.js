import { div, option, select } from '@hyperapp/html'

const sum = (x, y) => x + y

const getTotalWords = (verses, currentWord) =>
  verses
  .map(words => words.length)
  .concat(currentWord + 1)
  .reduce(sum, 0)

const getProgress = ({ verses, verse, word }) => {
  const progressWords = getTotalWords(verses.slice(0, verse), word)
  const totalWords = getTotalWords(verses, -1)
  return (progressWords / totalWords) * 100
}

const SPEEDS = Array.from(Array(20)).map((_, i) => (i + 1) * 50)

const createSpeedSelector = (state, actions, options) =>
  select({onchange: actions.setWPM, class: state.interval ? 'fade' : ''},
    options.map(value =>
      option({value, selected:state.wpm === value}, `${value}wpm`)
    )
  )

export default (state, actions) =>
  div({key:'progress', id:'progress'}, [
    div({class:'progress-bar'},
      div({
        class:'progress-indicator',
        style:{ 
          width:`${getProgress(state)}%` 
        }
      })
    ),
    createSpeedSelector(state, actions, SPEEDS)
  ])
