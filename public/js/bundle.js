(function () {
'use strict';

var i;
var stack = [];

function h(type, props) {
  var node;
  var children = [];

  for (i = arguments.length; i-- > 2; ) {
    stack.push(arguments[i]);
  }

  while (stack.length) {
    if (Array.isArray((node = stack.pop()))) {
      for (i = node.length; i--; ) {
        stack.push(node[i]);
      }
    } else if (node != null && node !== true && node !== false) {
      children.push(typeof node === "number" ? (node = node + "") : node);
    }
  }

  return typeof type === "string"
    ? { type: type, props: props || {}, children: children }
    : type(props || {}, children)
}

function app(props, container) {
  var root = (container = container || document.body).children[0];
  var node = toVNode(root, [].map);
  var callbacks = [];
  var skipRender;
  var globalState;
  var globalActions;

  repaint(flush(init(props, (globalState = {}), (globalActions = {}))));

  return globalActions

  function repaint() {
    if (props.view && !skipRender) {
      requestAnimationFrame(render, (skipRender = !skipRender));
    }
  }

  function render() {
    flush(
      (root = patchElement(
        container,
        root,
        node,
        (node = props.view(globalState, globalActions)),
        (skipRender = !skipRender)
      ))
    );
  }

  function flush(cb) {
    while ((cb = callbacks.pop())) cb();
  }

  function toVNode(element, map) {
    return (
      element &&
      h(
        element.tagName.toLowerCase(),
        {},
        map.call(element.childNodes, function(element) {
          return element.nodeType === 3
            ? element.nodeValue
            : toVNode(element, map)
        })
      )
    )
  }

  function init(module, state, actions) {
    if (module.init) {
      callbacks.push(function() {
        module.init(state, actions);
      });
    }

    assign(state, module.state);

    initActions(state, actions, module.actions);

    for (var i in module.modules) {
      init(module.modules[i], (state[i] = {}), (actions[i] = {}));
    }
  }

  function initActions(state, actions, source) {
    Object.keys(source || {}).map(function(i) {
      if (typeof source[i] === "function") {
        actions[i] = function(data) {
          return typeof (data = source[i](state, actions, data)) === "function"
            ? data(update)
            : update(data)
        };
      } else {
        initActions(state[i] || (state[i] = {}), (actions[i] = {}), source[i]);
      }
    });

    function update(data) {
      return (
        typeof data === "function"
          ? update(data(state))
          : data && repaint(assign(state, data)),
        state
      )
    }
  }

  function assign(target, source) {
    for (var i in source) {
      target[i] = source[i];
    }
    return target
  }

  function merge(target, source) {
    return assign(assign({}, target), source)
  }

  function createElement(node, isSVG) {
    if (typeof node === "string") {
      var element = document.createTextNode(node);
    } else {
      var element = (isSVG = isSVG || node.type === "svg")
        ? document.createElementNS("http://www.w3.org/2000/svg", node.type)
        : document.createElement(node.type);

      if (node.props && node.props.oncreate) {
        callbacks.push(function() {
          node.props.oncreate(element);
        });
      }

      for (var i = 0; i < node.children.length; i++) {
        element.appendChild(createElement(node.children[i], isSVG));
      }

      for (var i in node.props) {
        setElementProp(element, i, node.props[i]);
      }
    }
    return element
  }

  function setElementProp(element, name, value, oldValue) {
    if (name === "key") {
    } else if (name === "style") {
      for (var name in merge(oldValue, (value = value || {}))) {
        element.style[name] = value[name] || "";
      }
    } else {
      try {
        element[name] = value;
      } catch (_) {}

      if (typeof value !== "function") {
        if (value) {
          element.setAttribute(name, value);
        } else {
          element.removeAttribute(name);
        }
      }
    }
  }

  function updateElement(element, oldProps, props) {
    for (var i in merge(oldProps, props)) {
      var value = props[i];
      var oldValue = i === "value" || i === "checked" ? element[i] : oldProps[i];

      if (value !== oldValue) {
        value !== oldValue && setElementProp(element, i, value, oldValue);
      }
    }

    if (props && props.onupdate) {
      callbacks.push(function() {
        props.onupdate(element, oldProps);
      });
    }
  }

  function removeElement(parent, element, props) {
    if (
      props &&
      props.onremove &&
      typeof (props = props.onremove(element)) === "function"
    ) {
      props(remove);
    } else {
      remove();
    }

    function remove() {
      parent.removeChild(element);
    }
  }

  function getKey(node) {
    if (node && node.props) {
      return node.props.key
    }
  }

  function patchElement(parent, element, oldNode, node, isSVG, nextSibling) {
    if (oldNode == null) {
      element = parent.insertBefore(createElement(node, isSVG), element);
    } else if (node.type != null && node.type === oldNode.type) {
      updateElement(element, oldNode.props, node.props);

      isSVG = isSVG || node.type === "svg";

      var len = node.children.length;
      var oldLen = oldNode.children.length;
      var oldKeyed = {};
      var oldElements = [];
      var keyed = {};

      for (var i = 0; i < oldLen; i++) {
        var oldElement = (oldElements[i] = element.childNodes[i]);
        var oldChild = oldNode.children[i];
        var oldKey = getKey(oldChild);

        if (null != oldKey) {
          oldKeyed[oldKey] = [oldElement, oldChild];
        }
      }

      var i = 0;
      var j = 0;

      while (j < len) {
        var oldElement = oldElements[i];
        var oldChild = oldNode.children[i];
        var newChild = node.children[j];

        var oldKey = getKey(oldChild);
        if (keyed[oldKey]) {
          i++;
          continue
        }

        var newKey = getKey(newChild);

        var keyedNode = oldKeyed[newKey] || [];

        if (null == newKey) {
          if (null == oldKey) {
            patchElement(element, oldElement, oldChild, newChild, isSVG);
            j++;
          }
          i++;
        } else {
          if (oldKey === newKey) {
            patchElement(element, keyedNode[0], keyedNode[1], newChild, isSVG);
            i++;
          } else if (keyedNode[0]) {
            element.insertBefore(keyedNode[0], oldElement);
            patchElement(element, keyedNode[0], keyedNode[1], newChild, isSVG);
          } else {
            patchElement(element, oldElement, null, newChild, isSVG);
          }

          j++;
          keyed[newKey] = newChild;
        }
      }

      while (i < oldLen) {
        var oldChild = oldNode.children[i];
        var oldKey = getKey(oldChild);
        if (null == oldKey) {
          removeElement(element, oldElements[i], oldChild.props);
        }
        i++;
      }

      for (var i in oldKeyed) {
        var keyedNode = oldKeyed[i];
        var reusableNode = keyedNode[1];
        if (!keyed[reusableNode.props.key]) {
          removeElement(element, keyedNode[0], reusableNode.props);
        }
      }
    } else if (element && node !== element.nodeValue) {
      if (typeof node === "string" && typeof oldNode === "string") {
        element.nodeValue = node;
      } else {
        element = parent.insertBefore(
          createElement(node, isSVG),
          (nextSibling = element)
        );
        removeElement(parent, nextSibling, oldNode.props);
      }
    }
    return element
  }
}

const log = e => (console.log(e), e);
const err$1 = e => (console.error(e), e);

const ESV_OPTS = [
  `include-passage-references=false`,
  `include-footnotes=false`,
  `include-footnotes-body=false`,
  `include-copyright=false`,
  `include-short-copyright=false`,
  `include-passage-horizontal-lines=false`,
  `include-heading-horizontal-lines=false`,
  `include-headings=false`,
  `include-subheadings=false`,
  `include-content-type=false`,
  `indent-paragraphs=0`,
  `indent-poetry=0`,
  `indent-declares=0`
];

const buildQuery = (book, ch) =>
  ESV_OPTS
  .concat(`q=${book}+${ch}`)
  .join('&');

const buildESVEndpoint = (book, ch) =>
  `https://api.esv.org/v3/passage/text/?${buildQuery(book, ch)}`;

const headers = new Headers({ 
  Accept:'application/json',
  Authorization:'Token 6fbd080f49469fe46ff6fa9064664f8a46955edf'
});

const buildRequest = ({book, chapter}) =>
  new Request(buildESVEndpoint(book, chapter), {headers, method:'GET'});

const getPlayUpdate = ({verses, verse, word}) =>
  verses[verse][word + 1] ? { verse, word: word + 1 } :
  verses[verse + 1]       ? { verse: verse + 1, word: 0 } :
  { verse, word };

const getRewindUpdate = ({verses, verse, word}) =>
  verses[verse][word - 1] ? { verse, word: word - 1 } :
  verses[verse - 1]       ? { verse: verse - 1, word: verses[verse - 1].length - 1 } :
  { verse, word };

const getUpdateSpeed = wpm => 60000 / wpm;

const hasVerseNumber = s => /^\[/.test(s);

const splitAroundCenter = (str, length) => ([
  str.slice(0, length / 2),
  str.slice(length / 2, length / 2 + 1),
  str.slice(length / 2 + 1, length)
]);

const last = xs => xs[xs.length - 1];

const removeNewLine = str => str.replace(/\n/g, '');

const tokenize = (acc, word) =>
  hasVerseNumber(word) ?
    (acc.push([splitAroundCenter(word, word.length)]), acc) :
    appendToLast(acc, splitAroundCenter(word, word.length));

const appendToLast = (xs, element) =>
  ((last(xs) || []).push(element), xs);

const removeVerseNumbers = verse =>
  verse.filter(token => token.indexOf('[') === -1);

const processText = str =>
  str.split(' ')
  .filter(word => word !== '')
  .map(removeNewLine)
  .reduce(tokenize, [])
  .map(removeVerseNumbers);

var actions = {
  setWPM: (state, actions, {target:{value}}) => ({wpm:value}),
  menuToggle: (state, actions) => ({ menuOpen: !state.menuOpen }),
  select: (state, actions, book) => ({ selected: book }),
  play: (state, actions) => ({
    interval: setInterval(() => {
      actions.updateDisplay(getPlayUpdate(state));
    }, getUpdateSpeed(state.wpm))
  }),
  stop: (state, actions) => ({
    interval: clearInterval(state.interval)
  }),
  rewind: (state, actions) => ({
    interval: setInterval(() => {
      actions.updateDisplay(getRewindUpdate(state));
    }, getUpdateSpeed(state.wpm))
  }),
  updateBook: (state, actions, {book, chapter, verses}) => log({
    book, chapter, verses, verse:0, word:0, menuOpen: false
  }),
  updateDisplay: (state, actions, {verse, word}) => ({ verse, word }),
  fetchBook: (state, actions, data) => {
    fetch(buildRequest(data))
    .then(r => r.json())
    .then(({passages}) => passages[0])
    .then(blob => ({
      book:data.book, chapter:data.chapter, verses:processText(blob)
    }))
    .then(actions.updateBook)
    .catch(err$1);
  }
};

function vnode(tag) {
  return function (props, children) {
    return typeof props === "object" && !Array.isArray(props)
      ? h(tag, props, children)
      : h(tag, {}, props)
  }
}


























function button(props, children) {
  return vnode("button")(props, children)
}



























function div(props, children) {
  return vnode("div")(props, children)
}



















































function main(props, children) {
  return vnode("main")(props, children)
}



















function option(props, children) {
  return vnode("option")(props, children)
}



























function select(props, children) {
  return vnode("select")(props, children)
}





function span(props, children) {
  return vnode("span")(props, children)
}

const sum = (x, y) => x + y;

const getTotalWords = (verses, currentWord) =>
  verses
  .map(words => words.length)
  .concat(currentWord + 1)
  .reduce(sum, 0);

const getProgress = ({ verses, verse, word }) => {
  const progressWords = getTotalWords(verses.slice(0, verse), word);
  const totalWords = getTotalWords(verses, -1);
  return (progressWords / totalWords) * 100
};

const SPEEDS = Array.from(Array(20)).map((_, i) => (i + 1) * 50);

const createSpeedSelector = (state, actions, options) =>
  select({onchange: actions.setWPM, class: state.interval ? 'fade' : ''},
    options.map(value =>
      option({value, selected:state.wpm === value}, `${value}wpm`)
    )
  );

var progress$1 = (state, actions) =>
  div({key:'progress', id:'progress'}, [
    div({class:'progress-bar'},
      div({
        class:'progress-indicator',
        style:{ 
          width:`${getProgress(state)}%` 
        }
      })
    ),
    createSpeedSelector(state, actions, SPEEDS)
  ]);

var display = (state, actions) => {
  let word = state.verses[state.verse][state.word];
  return div({key:'display', id:'display'}, [
    span({id:'display-start'}, word[0]),
    span({id:'display-focus'}, word[1]),
    span({id:'display-end'}, word[2])
  ])
};

const preventDefaultThen = (fn) => (e) =>
  (e.preventDefault(), fn(e));

var toolbar = (state, actions) => {
  let menuBtnLabel = state.book ?
    `${state.book} ${state.chapter}:${state.verse + 1}` :
    'Menu';
  return div({key:'toolbar', id:'toolbar', class: state.interval ? 'fade' : ''}, [
    button({
      onmousedown: actions.rewind,
      onmouseup: actions.stop,
      ontouchstart: preventDefaultThen(actions.rewind),
      ontouchend: preventDefaultThen(actions.stop)
    }, '◄'),
    button({onclick: actions.menuToggle}, menuBtnLabel),
    button({
      onmousedown: actions.play,
      onmouseup: actions.stop,
      ontouchstart: preventDefaultThen(actions.play),
      ontouchend: preventDefaultThen(actions.stop)
    }, '►'),
  ])
};

var menu$1 = (state, actions) => 
  div({id:'menu', class: state.menuOpen ? 'show' : 'hide'},
    Object.keys(state.books)
    .map(book =>
      div({class:'menu-container'}, [
        button({
          class:'menu-book-button',
          onclick:() => actions.select(book),
        }, book),
        div({class:`menu-chapters ${state.selected === book ? 'show' : 'hide'}`},
          Array.from(Array(state.books[book]))
          .map((_, i) => i + 1)
          .map(chapter => 
            button({
              class:'menu-chapter-button button-clear',
              onclick:() => actions.fetchBook({book, chapter})
            }, chapter))
        )
      ])
    )
  );

var view = (state, actions) =>
  main({id:'main'}, [
    progress$1(state, actions),
    display(state, actions),
    toolbar(state, actions),
    menu$1(state, actions)
  ]);

const err = e => (console.error(e), e);

const state = {
  wpm: 250,
  book:'',
  chapter:0,
  verses:[[['RSV', 'P', 'ESV']]],
  verse:0,
  word:0
};

const booksData = fetch('data.json');

const init = books =>
  app({
    view,
    state:((state.books = books), state),
    actions
  });

window.onload = () =>
  booksData
  .then(r => r.json())
  .then(init)
  .catch(err);

}());
