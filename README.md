# RSVP reader for ESV Bible
(rapid serial visual presentation optimized for mobile screen)

[try it here!](https://heycato.gitlab.io/rsvpesv)

## To use:

Select your words per minute (wpm) then select a book and chapter from the menu.
Press and hold the right arrow button to "play", left arrow button to "rewind"